import os
import json
import shutil

from variables import tg_bot

import utils

from menus import get_show_games_header
from menus import create_show_games_menu

from menus import get_edit_game_header
from menus import create_edit_game_menu


def handle_game_info(command, chat_id, message_id, user):
    if command == 'go-back':
        tg_bot.edit_message_text(get_show_games_header(), chat_id=chat_id, message_id=message_id)
        tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id,
                                         reply_markup=create_show_games_menu())
        return str()
    elif command.startswith('notify_'):
        game_id = command.replace('notify_', '')

        tg_bot.edit_message_text("Хорошо! Напиши, что ты хочешь, чтобы я им отправил", chat_id=chat_id,
                                 message_id=message_id)

        if os.path.exists(f'./sessions/{user.id}'):
            shutil.rmtree(f'./sessions/{user.id}')
        os.makedirs(f'./sessions/{user.id}')
        session = dict()

        session["game_id"] = game_id
        session["action"] = "notify"
        session["stage"] = "compose"

        utils.save_to_file(f'./sessions/{user.id}/session.json', json.dumps(session, indent=4))

        return str()
    elif command.startswith('change-player-name_'):
        game_id = command.replace('change-player-name_', '')

        tg_bot.edit_message_text("Хорошо! Напиши, как ты хочешь, чтобы я тебя подписывал", chat_id=chat_id,
                                 message_id=message_id)

        if os.path.exists(f'./sessions/{user.id}'):
            shutil.rmtree(f'./sessions/{user.id}')
        os.makedirs(f'./sessions/{user.id}')

        session = dict()
        session["game_id"] = game_id
        session["action"] = "change-player-name"
        session["stage"] = "compose"

        utils.save_to_file(f'./sessions/{user.id}/session.json', json.dumps(session, indent=4))

        return str()
    elif command.startswith('open-reg_'):
        game_id = command.replace('open-reg_', '')

        f = open(f'./games/{game_id}.json', encoding='utf-8')
        game_json = json.load(f)
        f.close()

        game_json["registration_open"] = True
        utils.save_to_file(f'./games/{game_id}.json', json.dumps(game_json, indent=4))

        tg_bot.edit_message_text(get_show_games_header(), chat_id=chat_id, message_id=message_id)
        tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id,
                                         reply_markup=create_show_games_menu())

        return "Регистрация открыта!"
    elif command.startswith('close-reg_'):
        game_id = command.replace('close-reg_', '')

        f = open(f'./games/{game_id}.json', encoding='utf-8')
        game_json = json.load(f)
        f.close()

        game_json["registration_open"] = False
        utils.save_to_file(f'./games/{game_id}.json', json.dumps(game_json, indent=4))

        tg_bot.edit_message_text(get_show_games_header(), chat_id=chat_id, message_id=message_id)
        tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id,
                                         reply_markup=create_show_games_menu())

        return "Регистрация закрыта!"
    elif command.startswith('bump-session_'):
        game_id = command.replace('bump-session_', '')

        f = open(f'./games/{game_id}.json', encoding='utf-8')
        game_json = json.load(f)
        f.close()

        game_json["session"] += 1
        utils.save_to_file(f'./games/{game_id}.json', json.dumps(game_json, indent=4))

        tg_bot.edit_message_text(get_show_games_header(), chat_id=chat_id, message_id=message_id)
        tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id,
                                         reply_markup=create_show_games_menu())

        return "Номер сессии увеличен!"
    elif command.startswith('edit_'):
        game_id = command.replace('edit_', '')

        tg_bot.edit_message_text(get_edit_game_header(), chat_id=chat_id, message_id=message_id)
        tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id,
                                         reply_markup=create_edit_game_menu(game_id))

        return str()
    elif command.startswith('delete_'):
        game_id = command.replace('delete_', '')

        tg_bot.edit_message_text('Хорошо! Напиши "Подтверждаю", если ты уверен(а). Если напишешь что-то другое - удаление будет отменено.', chat_id=chat_id,
                                 message_id=message_id)

        if os.path.exists(f'./sessions/{user.id}'):
            shutil.rmtree(f'./sessions/{user.id}')
        os.makedirs(f'./sessions/{user.id}')
        session = dict()

        session["game_id"] = game_id
        session["action"] = "delete"
        session["stage"] = "confirm"

        utils.save_to_file(f'./sessions/{user.id}/session.json', json.dumps(session, indent=4))

        return str()
    elif command.startswith('register_'):
        game_id = command.replace('register_', '')
        f = open(f'./games/{game_id}.json', encoding='utf-8')
        game_json = json.load(f)
        f.close()

        game_json["players"].append(user.id)
        game_json["player_names"].append(f'{user.first_name} {user.last_name}')
        game_json["free_slots"] -= 1

        if game_json["free_slots"] == 0:
            game_header = f'{game_json["system_short"]}]{game_json["campaign"]} - {game_json["scenario"]}'
            tg_bot.send_message(game_json["gm"], f"Привет! На твою игру записалось максимальное количество игроков!\n{game_header}")

        utils.save_to_file(f'./games/{game_id}.json', json.dumps(game_json, indent=4))

        tg_bot.edit_message_text(get_show_games_header(), chat_id=chat_id, message_id=message_id)
        tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id,
                                         reply_markup=create_show_games_menu())

        return 'Вы успешно записались на игру!'
    elif command.startswith('unregister_'):
        game_id = command.replace('unregister_', '')
        f = open(f'./games/{game_id}.json', encoding='utf-8')
        game_json = json.load(f)
        f.close()

        for i in range(len(game_json["players"])):
            if game_json["players"][i] == user.id:
                del game_json["players"][i]
                del game_json["player_names"][i]
                break
        game_json["free_slots"] += 1
        utils.save_to_file(f'./games/{game_id}.json', json.dumps(game_json, indent=4))

        tg_bot.edit_message_text(get_show_games_header(), chat_id=chat_id, message_id=message_id)
        tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id,
                                         reply_markup=create_show_games_menu())

        return 'Вы успешно отменили запись на игру!'
