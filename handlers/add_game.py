import json
import shutil

from variables import tg_bot

import utils


def handle_add_game(text, chat_id, user_id):
    f = open(f'./sessions/{user_id}/session.json', encoding='utf-8')
    session = json.load(f)
    f.close()

    f = open(f'./sessions/{user_id}/game_info.json', encoding='utf-8')
    game_json = json.load(f)
    f.close()

    if session["stage"] == 'system_short':
        game_json["system_short"] = text
        tg_bot.send_message(chat_id, 'Отлично, теперь напиши полное название системы, например "Dungeons & Dragons 5-я редакция"')
        session["stage"] = 'system_full'
    elif session["stage"] == 'system_full':
        game_json["system_full"] = text
        tg_bot.send_message(chat_id, 'Отлично, теперь напиши название кампании, например "The Wild Beyond The Witchlight" или "Ваншот", если это - ваншот')
        session["stage"] = 'campaign'
    elif session["stage"] == 'campaign':
        game_json["campaign"] = text
        tg_bot.send_message(chat_id, 'Отлично, теперь напиши, название сценария, например "Неожиданное знакомство"')
        session["stage"] = 'scenario'
    elif session["stage"] == 'scenario':
        game_json["scenario"] = text
        tg_bot.send_message(chat_id, 'Отлично, теперь напиши, какое максимальное количество игроков может записаться. Это должно быть одно число, без дополнительных символов, например "3"')
        session["stage"] = 'slots'
    elif session["stage"] == 'slots':
        try:
            slots = int(text)
        except:
            tg_bot.send_message(chat_id, 'Упс! Кажется, ты отправил(а) что-то кроме числа. Количество мест на игре должно быть числом без дополнительных символов')
            return

        game_json["slots"] = slots
        game_json["free_slots"] = slots
        tg_bot.send_message(chat_id, 'Отлично, теперь напиши, когда состоится игра. Если не знаешь, можешь написать "TBD", потом можно будет изменить')
        session["stage"] = 'date'
    elif session["stage"] == 'date':
        game_json["date"] = text
        tg_bot.send_message(chat_id, 'Отлично, вводную к своей игре. Можешь написать завязку сюжета, какие-то свои правила и всё, что посчитаешь нужным.')
        session["stage"] = 'intro'
    elif session["stage"] == 'intro':
        game_json["intro"] = text
        session["stage"] = 'finished'
        shutil.rmtree(f'./sessions/{user_id}/')

    if session["stage"] == 'finished':
        game_id = int(utils.get_from_file('./latest_game')) + 1

        utils.save_to_file(f'./games/{game_id}.json', json.dumps(game_json))
        utils.save_to_file('./latest_game', game_id)

        tg_bot.send_message(chat_id, 'Отлично! Я всё записал')
    else:
        utils.save_to_file(f'./sessions/{user_id}/session.json', json.dumps(session, indent=4))
        utils.save_to_file(f'./sessions/{user_id}/game_info.json', json.dumps(game_json, indent=4))

    return
