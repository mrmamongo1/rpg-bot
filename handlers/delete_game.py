import json
import os
import shutil

from variables import tg_bot


def handle_delete_game(text, chat_id, user_id):
    if text == "Подтверждаю":
        f = open(f'./sessions/{user_id}/session.json', encoding='utf-8')
        session = json.load(f)
        f.close()
        os.remove(f'./games/{session["game_id"]}.json')
        tg_bot.send_message(chat_id, 'Игра удалена')
    else:
        tg_bot.send_message(chat_id, 'Вы отправили сообщение, отличное от "Подтверждаю" - удаление игры отменено')

    shutil.rmtree(f'./sessions/{user_id}')
