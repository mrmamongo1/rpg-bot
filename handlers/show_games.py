from variables import tg_bot

from menus import get_main_menu_header
from menus import create_main_menu

from menus import get_game_info_header
from menus import create_game_info_menu


def handle_show_games(command, chat_id, message_id, user):
    if command == 'go-back':
        tg_bot.edit_message_text(get_main_menu_header(), chat_id=chat_id, message_id=message_id)
        tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id,
                                         reply_markup=create_main_menu())
        return str()
    else:
        game_id = command

        tg_bot.edit_message_text(get_game_info_header(game_id), chat_id=chat_id, message_id=message_id, parse_mode='HTML')
        tg_bot.edit_message_reply_markup(chat_id=chat_id, message_id=message_id,
                                         reply_markup=create_game_info_menu(game_id, user))
        return str()
