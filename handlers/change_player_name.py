import json
import shutil

from variables import tg_bot

import utils


def handle_change_player_name(text, chat_id, user_id):
    f = open(f'./sessions/{user_id}/session.json', encoding='utf-8')
    session = json.load(f)
    f.close()

    if session["stage"] == 'compose':
        session['new_name'] = text
        session["stage"] = 'confirm'
        utils.save_to_file(f'./sessions/{user_id}/session.json', json.dumps(session, indent=4))

        tg_bot.send_message(chat_id, 'Отлично! Проверь ещё раз и напиши "Подтверждаю", чтобы применить изменения. Если отправишь что-то другое - изменения будут отменены')
    elif session["stage"] == 'confirm':
        if text != 'Подтверждаю':
            tg_bot.send_message(chat_id, 'Вы отправили сообщение, отличное от "Подтверждаю" - редактирование игры отменено')
        else:
            game_id = session["game_id"]
            f = open(f'./games/{game_id}.json', encoding='utf-8')
            game_json = json.load(f)
            f.close()

            for i in range(len(game_json["players"])):
                if game_json["players"][i] == user_id:
                    game_json["player_names"][i] = session['new_name']
                    break
            utils.save_to_file(f'./games/{game_id}.json', json.dumps(game_json, indent=4))

        tg_bot.send_message(chat_id, 'Ваше имя обновлено!')
        shutil.rmtree(f'./sessions/{user_id}')
